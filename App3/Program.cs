﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaNegocio;

namespace App3
{
    class Program
    {
        static void Main(string[] args)
        {

            Validacion Negocio = new Validacion();
            System.Console.WriteLine("Ingrese el nombre:");

            System.Console.WriteLine("Ingrese los datos");
            Negocio.IngresarVenta(1, "111111", 1);


            foreach (var item in Negocio.RetornarVentas())
            {
                System.Console.WriteLine(item.CedulaCliente);
            }
            System.Console.WriteLine();
            System.Console.WriteLine();

            Validacion validacion = new Validacion();


            validacion.RetornarVentas();

            System.Console.WriteLine("CEDULA\t\tPRODUCTO\tCANTIDAD");
            foreach (var item in validacion.RetornarVentas())
            {
                System.Console.WriteLine(item.CedulaCliente + "\t\t" + item.idProducto + "\t\t" + item.Cantidad);
            }

            System.Console.WriteLine();
            System.Console.WriteLine();


            validacion.Ordenar();
            System.Console.WriteLine("CEDULA\t\tPRODUCTO\tCANTIDAD");

            foreach (var item2 in validacion.Ordenar())
            {
                System.Console.WriteLine(item2.CedulaCliente + "\t\t" + item2.idProducto + "\t\t" + item2.Cantidad);
            }

            System.Console.WriteLine();
            System.Console.WriteLine();

            var consulta =
                validacion.Ordenar().OrderByDescending(Od => Od.Cantidad).Select(x => new { x.CedulaCliente, x.idProducto, x.Cantidad });

            foreach (var item in consulta)
            {
                System.Console.WriteLine(item.CedulaCliente + "\t\t" + item.idProducto + "\t\t" + item.Cantidad);
            }
            System.Console.WriteLine();
            System.Console.WriteLine();


            var mayor = validacion.Ordenar().OrderByDescending(Od => Od.Cantidad).Select(x => new { x.idProducto });


            {
                System.Console.WriteLine("el nombre del producto que mas se ha comprado es: " + mayor.FirstOrDefault().idProducto);
            }
            Console.WriteLine();
            Console.WriteLine();

            validacion.ObtenerVentas();
            var group =
               validacion.ObtenerVentas().GroupBy(X => X.CedulaCliente).Select(g => new
               {
                   CantidadSumada = g.Sum(s => s.Cantidad),
                   Cedula = g.First().CedulaCliente,
                   IdProducto = g.First().idProducto
               });
            validacion.ObtenerClientes();
            var DatosCliente =
             validacion.ObtenerClientes().GroupJoin(validacion.ObtenerClientes(), x => x.Cedula,
             y => y.Cedula, (x, y) => x.Apellidos + " " + x.Cedula);


            Console.WriteLine("CEDULA\t\tPRODUCTO\tCANTIDAD SUMADA");
            foreach (var item in group)
            {
                Console.WriteLine(item.Cedula + "\t\t" + item.IdProducto + "\t\t" + item.CantidadSumada);
            }

            Console.WriteLine();
            Console.WriteLine();

            foreach (var item in DatosCliente)
            {
                Console.WriteLine(item + "\t\t");
            }
            Console.WriteLine();
            Console.WriteLine();
            // agregar la propiedad fecha en ventas



            validacion.ObtenerProductos();



            var Consulta15 =

                validacion.ObtenerProductos().OrderBy(x => x.Precio).Select(x => new
                {

                    x.Identificador,
                    x.Descripcion,
                    x.Precio
                });


            foreach (var item in Consulta15)
            {
                Console.WriteLine("PRECIO\t\t" + item.Precio + "\t\tIDENTIFICADOR\t" + item.Identificador +
                    "\t\tDESCRIPCION\t" + item.Descripcion);
            }
            // ordenar de menor a mayor

            Console.WriteLine();
            Console.WriteLine();

            //validacion.Ordenar2();
            //System.Console.WriteLine("t\tPRECIO\t\tIDENTIFICADORt\tDESCRIPCION\t");

            //foreach (var item2 in validacion.Ordenar())
            //{
            //    System.Console.WriteLine(item2.CedulaCliente + "\t\t" + item2.idProducto + "\t\t" + item2.Cantidad);
            //}

            //System.Console.WriteLine();
            //System.Console.WriteLine();

            var consulta4 =
                validacion.Ordenar2().OrderByDescending(Od => Od.Precio).Select(x => new { x.Precio, x.Descripcion, x.Identificador });

            foreach (var item in consulta4)
            {
                System.Console.WriteLine("\t\tPRECIO\t" + item.Precio + "\t\tIDENTIFICADOR\t" + item.Identificador + "\t\tDESCRIPCION" + item.Descripcion);
            }
            System.Console.WriteLine();
            System.Console.WriteLine();

            validacion.ObtenerProductos();
            var consulta222 =
                validacion.ObtenerProductos().OrderBy(x => x.Identificador).Select(y => new
                {
                    y.Identificador,
                    y.Precio,
                    y.Descripcion,
                    y.BuscarID
                });
           



            //mostrar el producto con el precio mayor

            var mayor2 = validacion.Ordenar2().OrderByDescending(Od => Od.Precio).Select(x => new { x.Precio,x.Descripcion,x.Identificador });

            Console.WriteLine("el producto con el precio mayor es: ");

            {
                mayor2.Take(1).ToList().ForEach(y =>
                
                  Console.WriteLine("\t\tprecio" + y.Precio + "\t\t" + "\t\tidentificador" + y.Identificador + "\t\tdescripcion" + y.Descripcion));
            }
            Console.WriteLine();
            Console.WriteLine();
            
            //mostar el producto con el pecio menor
            var menor2 = validacion.Ordenar2().OrderBy(Od => Od.Precio).Select(x => new { x.Precio,x.Descripcion,x.Identificador });

            Console.WriteLine("el producto con el precio menor es: ");
            {
                menor2.Take(1).ToList().ForEach(y =>
                  Console.WriteLine("\t\tprecio" + y.Precio + "\t\t" + "\t\tidentificador"+ y.Identificador + "\t\tdescripcion" + y.Descripcion));

            }
            Console.WriteLine();
            Console.WriteLine();

           
            // mostrar por consola los 3 clientes con menos precios en ventas:
            //   validacion.RetornarVentas();



            System.Console.WriteLine("CEDULA\t\tPRODUCTO\tCANTIDAD\tAPELLIDOS\tNOMBRES");
            foreach (var item in validacion.RetornarVentas())
            {
                System.Console.WriteLine(item.CedulaCliente + "\t\t" + item.idProducto + "\t\t" + item.Cantidad);
            }
            Console.WriteLine();
            Console.WriteLine();

            var consulta6 =
         validacion.ObtenerVentas().OrderByDescending(x => x.Cantidad).Select(x => new
         {
             x.Cantidad,
             x.CedulaCliente,
             x.idProducto
         });


            validacion.ObtenerClientes().OrderByDescending(x => x.Nombres).Select(x => new
            {
                x.Nombres,
                x.Apellidos,
                x.Cedula
            });
            {
                foreach (var item in consulta6)
                {
                    System.Console.WriteLine("CEDULA\t\tPRODUCTO\tCANTIDAD\tAPELLIDOS\tNOMBRES" + item);
                }
                Console.WriteLine();
                Console.WriteLine();

                var MayorMonto =

             validacion.ObtenerVentas().OrderBy(Od => Od.Cantidad).Select(x => new
             {
                 x.Cantidad,
                 x.CedulaCliente,
                 x.idProducto
             });

                var MayorCliente =

                validacion.ObtenerClientes().OrderByDescending(Od => Od.Apellidos).Select(x => new
                {
                    x.Apellidos,
                    x.Nombres,
                    x.Cedula
                });
                {

                    //    validacion.RetornarVentas();
                    //    { 
                    //    System.Console.WriteLine("CEDULA\t\tPRODUCTO\tCANTIDAD\tAPELLIDOS\tNOMBRES");
                    //}
                    MayorMonto.Take(3).ToList().ForEach(x =>

                          System.Console.WriteLine("LOS 3 CLIENTES CON MENOR PRECIOS EN VENTA SON :\t\tCEDULA\t" +
                             x.CedulaCliente + "\t\tPRODUCTO\t" + x.idProducto + "\t\tCANTIDAD\t" + x.Cantidad));

                    MayorCliente.Take(3).ToList().ForEach(y =>
                     Console.WriteLine("\t\tNOMBRES" + y.Nombres + "\t\t" + "\t\tAPELLIDOS" + y.Apellidos + "\t\tCEDULA" +y.Cedula));


                }
                Console.WriteLine();
                Console.WriteLine();
                //validacion.ObtenerClientes();




                // mostrar por consola los 3 clientes con mas precios en ventas:
                var MenorMonto =

                 validacion.ObtenerVentas().OrderByDescending(Od => Od.Cantidad).Select(x => new { x.Cantidad, x.CedulaCliente, x.idProducto });


                var MenorCliente =

            validacion.ObtenerClientes().OrderBy(Od => Od.Apellidos).Select(x => new
            {
                x.Apellidos,
                x.Nombres,
                x.Cedula
            });
                Console.WriteLine();
                Console.WriteLine();
                {

                    MenorMonto.Take(3).ToList().ForEach(x =>

                    System.Console.WriteLine("LOS 3 CLIENTES CON MAYOR PRECIOS EN VENTA SON :\t\tCEDULA\t" +
                             x.CedulaCliente + "\t\tPRODUCTO\t" + x.idProducto + "\t\tCCANTIDAD\t" + x.Cantidad));

                    MenorCliente.Take(3).ToList().ForEach(y =>
                   Console.WriteLine("\t\tNOMBRES" + y.Nombres + "\t\t" + "\t\tAPELLIDOS" + y.Apellidos + "\t\tCEDULA" + y.Cedula));

                }
                //       //   Console.WriteLine();
                //       //   Console.WriteLine();


                Console.WriteLine();
                Console.WriteLine();

               



                //Mostar en consola las ventas realizadas hace menos de un año

                validacion.ObtenerVentas();

                var Consulta12 = validacion.ObtenerVentas().OrderByDescending(x => x.Fecha).Select(x => new { x.Cantidad, x.Fecha, x.CedulaCliente });

                foreach (var item in Consulta12)
                {
                    Console.WriteLine("ingresar fecha en formato 00// 02// 2020//" + item.Fecha);
                }
                Console.ReadLine();



                //Console.ReadLine();

                // foreach (var item in Negocio.RetornarCliente())
                // {
                //     Console.WriteLine(item.Nombres);
                // }
                // foreach (var item in Negocio.RetornarVentas())
                // {
                //     Console.WriteLine(item.idProducto);
            }

            //string nombre;
            //int producto;
            //int venta;

            //CapaDatos.GestionClientes clientes = new CapaDatos.GestionClientes();
            //clientes.IngresarCliente();
            //Console.WriteLine("Ingrese el Nombre del cliente:");
            //nombre = Console.ReadLine();


            //CapaDatos.GestionProductos gestionProductos = new CapaDatos.GestionProductos();
            //gestionProductos.IngresarProducto();
            //Console.WriteLine("Ingrese el producto:");
            //producto = Int32.Parse(Console.ReadLine());


            //CapaDatos.GestionVentas gestionVentas = new CapaDatos.GestionVentas();
            //gestionVentas.ObtenerVentas();
            //Console.WriteLine("Ingrese  la venta:");
            //venta = Int32.Parse(Console.ReadLine());

            //producto venta cliente

            // cuales son los productos mas vendidos
        }
    }

}
