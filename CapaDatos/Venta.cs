﻿using System;
namespace CapaDatos
{
    public class Venta
    {
        public string CedulaCliente { get; set; }
        public int idProducto { get; set; }
        public int Cantidad { get; set; }
        public DateTime Fecha { get; set; }
        public Venta()
        {
        }
    }
}
