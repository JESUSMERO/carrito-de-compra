﻿using System;
namespace CapaDatos
{
    public class Cliente
    {
        public string Cedula { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public Cliente()
        {
        }
    }
}
