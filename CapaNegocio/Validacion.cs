﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CapaDatos;

namespace CapaNegocio
{
    public class Validacion
    {
        GestionVentas gestionVentas = new GestionVentas();

        public List<Venta> RetornarVentas()
        {
            return gestionVentas.ObtenerVentas();
        }


        public void IngresarVenta(int cantidad, string cedula, int idProducto)
        {
            Venta venta = new Venta();
            if (cantidad > 0)
            {
                venta.Cantidad = cantidad;
                venta.CedulaCliente = cedula;
                venta.idProducto = idProducto;


            }
            gestionVentas.IngresarVenta(venta);

        }
        public List<Venta> Ordenar()
        {
            return gestionVentas.ObtenerVentas();
        }
        public void IngresarFecha(int cantidad, string cedula, int idProducto, DateTime fecha)
        {
            Venta venta = new Venta();
            if (cantidad > 0)
            {
                venta.Cantidad = cantidad;
                venta.CedulaCliente = cedula;
                venta.idProducto = idProducto;
                venta.Fecha = fecha;


            }
            gestionVentas.IngresarFecha(fecha);
        }

        public List<Venta> ObtenerVentas()
        {
            return this.gestionVentas.ObtenerVentas();
        }

        GestionClientes gestionclientes = new GestionClientes();

        public List<Cliente> RetornarClientes()
        {
            return this.gestionclientes.ObtenerClientes();
        }
        public List<Cliente> ObtenerClientes()
        {
            return this.gestionclientes.ObtenerClientes();
        }

        public void IngresarClientes(string apellidos, string cedula, string nombres)
        {
            Cliente cliente = new Cliente();

            {
                cliente.Apellidos = apellidos;
                cliente.Nombres = nombres;
                cliente.Cedula = cedula;

            }
            gestionclientes.ObtenerClientes();


        }
        GestionProductos gestionproductos = new GestionProductos();

        public List<Producto> RetornarProductos()
        {
            return this.gestionproductos.ObtenerProductos();
        }
        public List<Producto> ObtenerProductos()
        {
            return this.gestionproductos.ObtenerProductos();
        }

        public void IngresarProductos(int identificador, string descripcion2, int precio)
        {
            Producto producto = new Producto();

            {
                producto.Identificador = identificador;
                producto.Descripcion = descripcion2;
                producto.Precio = precio;

            }
            gestionproductos.ObtenerProductos();

        }
        public List<Producto> Ordenar2()
        {
            return gestionproductos.ObtenerProductos();
        }
        public void BuscarIdProductos(int buscarid)
        {
            Producto buscar = new Producto();
            buscar.Identificador = buscarid;

            gestionproductos.BuscarIdProductos();
        }
      

        // agregar la propiedad fecha en ventas

        //mostrar en consola los 3 clientes con mas monot en ventas.


        //public List<Producto> RetornarProductos();
        //{
        //      return 
        //}
        //
    }
    }


